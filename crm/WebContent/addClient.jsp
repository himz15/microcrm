<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.io.*" %>
<!DOCTYPE html>
<html>
<head>
	<title>Add Client</title>
	
	<link rel="stylesheet" href="style.css">
	<style>
#header {
    background-color:black;
    color:white;
    text-align:center;
    padding:5px;
}
#nav {
    line-height:30px;
    background-color:#eeeeee;
    height:300px;
    width:100px;
    float:left;
    padding:5px;
}
#section {
    width:350px;
    float:left;
    padding:10px;
}
#footer {
    background-color:black;
    color:white;
    clear:both;
    text-align:center;
    padding:5px;
}
</style>
</head>

<body>
<div id="header">
<h1>Add Client</h1>
<h3>
<a href="/crm/index.html">CRM Home</a>
</h3>
</div>

	<section class="loginform cf">
	<%
	String status = request.getParameter("status");
	
	if(status != null)
	{
	%>
	<p>Add Client operation was a <%=status%></p>	
	<%
	} 
	%>
		<form name="add" action="saveClient.jsp" method="get" accept-charset="utf-8">
			<ul>
				<li>
					<label for="username">Username</label>
					<input type="text" name="username" placeholder="username" required="required" pattern="[A-Z0-9]+"/ title="username should be of type USR*** only" >
				</li>
				<li>
					<label for="password">Password</label>
					<input type="password" name="password" placeholder="password" required="required" ></li>
				<li>
					<label for="firstname">Firstname</label>
					<input type="text" name="firstname" placeholder="firstname" required="required" pattern="[A-Za-z]+"/ title="Firstname should be of type character only">
				</li>
				<li>
					<label for="lastname">Lastname</label>
					<input type="text" name="lastname" placeholder="lastname" required="required" pattern="[A-Za-z]+"/ title="lastname should be of type character only">
				</li>
				
				
				<li>
					<label for="mobile">Mobile</label>
					<input type="text" name="mobile" placeholder="mobile no" required="required"  >
				</li>
				<li>
					<label for="email">Email</label>
					<input type="email" name="email" placeholder="username@example.com" required>
				</li>
				<li>
					<label for="gender">Gender</label>
					<input type="text" name="gender" placeholder="gender" required>
				</li>
				
				
				<li>
					<input type="submit" value="ADD details" >
					 <button type="reset" value="Clear">Clear</button>
				</li>
			</ul>
		</form>
	</section>
	<div id="footer">
Copyright � Himanshu Upadhyay
</div>
<script language="javascript">
 var today = new Date();
 document.write(today);
 </script>

</body>
</html>