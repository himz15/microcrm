<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*"%>

<%@page import="javax.sql.*" %>

<%@page import="java.sql.Connection" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="javax.ws.rs.core.MediaType"%>

<%@page import="com.sun.jersey.api.client.Client"%>
<%@page import="com.sun.jersey.api.client.ClientResponse"%>
<%@page import="com.sun.jersey.api.client.WebResource"%>
<%@page import="com.sun.jersey.api.client.filter.HTTPBasicAuthFilter"%>
<%@page import="com.sun.jersey.core.util.MultivaluedMapImpl"%>


<!DOCTYPE html>
<html>
<head>
	<title>Email Sent History</title>
	
	<link rel="stylesheet" href="style.css">
	<style>
#header {
    background-color:black;
    color:white;
    text-align:center;
    padding:5px;
}
#nav {
    line-height:30px;
    background-color:#eeeeee;
    height:300px;
    width:100px;
    float:left;
    padding:5px;
}
#section {
    width:350px;
    float:left;
    padding:10px;
}
#footer {
    background-color:black;
    color:white;
    clear:both;
    text-align:center;
    padding:5px;
}
</style>
</head>

<body>
<div id="header">
<h1>Email Sent History</h1>
<h3>
<a href="/crm/index.html">CRM Home</a>
</h3>
</div>

	<section class="loginform cf" style="width:800px;">
<!-- Show Send Email Confirmation -->
<table border="2" bordercolor="#2494b7">

<tr>
<th>Client Group ID</th>
<th>Client Group Name</th>
<th>Client ID</th>
<th>Client Email</th>
<th>Client Group Email Status</th>
<th>Last Email Sent Time</th>
</tr>

<%
//Creating JDBC Connection to MySQL
Class.forName("com.mysql.jdbc.Driver");

Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","himanshu");
// Get All selected client group IDs
String groupIDs[] = request.getParameterValues("group_ids");

for(int i = 0; i<groupIDs.length;i++) 
{
	int updateStatus;
	Statement st1=con.createStatement();
	String sqlGroupClientEmails = "select distinct(c.email),c.client_id from client c, client_group_mapping cgm where c.client_id = cgm.client_id and cgm.client_group_id = " + groupIDs[i];
	String query="select distinct(cg.client_group_id),cg.group_name,cge.client_id,c.email,cge.status,cge.sent_time from client c, client_group cg,client_group_email cge where c.client_id = cge.client_id and cg.client_group_id = cge.group_id and cge.group_id = " + groupIDs[i] + " order by cge.sent_time desc";
	List<String> sqlInsert = new ArrayList<String>();

	try
	{
		ResultSet rs1=st1.executeQuery(sqlGroupClientEmails);
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		
		while(rs1.next())
		{
			// Sending email via Mailgun API
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter("api","key-1b83e868ff63ce7d4c7f42286f1b7f31"));
			WebResource webResource = client.resource("https://api.mailgun.net/v3/sandbox70f515043a8a4215b76d02caeee8931c.mailgun.org" + "/messages");
			formData.add("from", "himz15@himz.mailgun.com");
			formData.add("to", "himanshuu731@gmail.com");
			formData.add("to", "pranavthakur82@gmail.com");
			formData.add("subject", "Himz Mailgun Emails");
			formData.add("text", "Testing Himz Mailgun Emails!");
			formData.add("to", rs1.getString(1));
			
			ClientResponse clResp = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
		            post(ClientResponse.class, formData);
			
			if(clResp.getStatus() == 200)// if email sent successfully
			{
				sqlInsert.add("insert into client_group_email(client_id,group_id,status) values("+ rs1.getString(2) + "," + groupIDs[i] + ",'y')");
			}
			else
			{
				sqlInsert.add("insert into client_group_email(client_id,group_id,status) values("+ rs1.getString(2) + "," + groupIDs[i] + ",'n')");
			}
		}
		
		
	}
	catch(Exception e)
	{
		out.println("rs1 exception");
	}

	// Printing History of all Emails sent to selected groups
	Statement st2 = con.createStatement();
	for(String sql:sqlInsert)
	{
		updateStatus = st2.executeUpdate(sql);	
	}
	
	Statement st3 = con.createStatement();
	ResultSet rs2 = null;
	rs2=st3.executeQuery(query);

	while(rs2.next())
	{
	%>
	
	<tr>

	<td><%=rs2.getString(1)%></td>
	
	<td><%=rs2.getString(2)%></td>
	
	<td><%=rs2.getString(3)%></td>
	
	<td><%=rs2.getString(4)%></td>
	
	<td><%=rs2.getString(5)%></td>
	
	<td><%=rs2.getString(6)%></td>
	
	</tr>
	<%
	}
}
%>

</table>
</section>

<a href="/crm/selectGroup.jsp">Send More Emails</a>
	<div id="footer">
Copyright � Himanshu Upadhyay
</div>
<script language="javascript">
 var today = new Date();
 document.write(today);
 </script>

</body>
</html>