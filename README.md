This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* Quick summary: This Repository is for CRM project with bulk email sending utility
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Summary of set up: 
1.	Code and Documentation is available on https://bitbucket.org/himz15/hrcausecode.git 
2.	Setup Eclipse and Tomcat
3.	Setup MySQL in Eclipse and Tomcat
4.	Check the port of Tomcat
5.	Deploy crm.war into <Tomcat Home>\webapps\
6.	Start Tomcat
7.	Open http://localhost:<port>/crm/index.html 
* Configuration
1.	You may skip installing Eclipse and deploy the “crm.war” file directly on Tomcat
* Dependencies
* Database configuration
1.	Open MySQL Console and login with your credentials and fire the commands given below
2.	CREATE DATABASE db;//creates DB
3.	Use db;
4.	CREATE TABLE `client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `mobile` int(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gender` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


CREATE TABLE `client_group` (
  `client_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


5.	CREATE TABLE `client_group_email` (
  `cge_id` int(11) NOT NULL AUTO_INCREMENT,
  `sent_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(1) DEFAULT 'n',
  `group_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cge_id`),
  KEY `fk_group_email` (`group_id`),
  KEY `fk_client_email` (`client_id`),
  CONSTRAINT `client_group_email_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `cl
ient_group` (`client_group_id`),
  CONSTRAINT `client_group_email_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `c
lient` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

6.	CREATE TABLE `client_group_mapping` (
  `client_id` int(11) NOT NULL,
  `client_group_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`,`client_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 |

* How to run tests
1.	Add one or more Clients
2.	Add one or more Client Groups
3.	Try sending emails to one or more Client Groups
4.	Check Email Sent History details of selected Client Groups

* Deployment instructions
1.	Database setup must be completed first
2.	Load “crm.war” file using Eclipse and run it on Tomcat or Jetty

### Who do I talk to? ###
* Repo owner or admin: Himanshu Upadhyay <himanshuu731@gmail.com>